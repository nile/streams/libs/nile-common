# Nile Common

**Nile Common** is a Java library designed for building Kafka streaming applications. This library
encapsulates a range of functionalities including stream processing, schema management, serialization & deserialization,
and Health checks, thus providing a robust foundation for developing data streaming solutions.

## Getting Started

### Prerequisites

- Java 21 or higher

### Adding Dependency

Currently, the library is available as a maven artifact. To add the dependency to your project, add the following to
your `pom.xml` file:

```xml

<dependency>
    <groupId>ch.cern.nile</groupId>
    <artifactId>nile-common</artifactId>
    <version>1.0.0</version>
</dependency>
```

Since this library is not available on Maven Central, you will need to also set up the registry in your `pom.xml`
file:

```xml
<repositories>
    <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.cern.ch/api/v4/projects/170995/packages/maven</url>
    </repository>
</repositories>
```

and 

```xml
<distributionManagement>
<repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.cern.ch/api/v4/projects/170995/packages/maven</url>
</repository>

<snapshotRepository>
    <id>gitlab-maven</id>
    <url>https://gitlab.cern.ch/api/v4/projects/170995/packages/maven</url>
</snapshotRepository>
</distributionManagement>
```

## Basic Usage

### Extending AbstractStream

Extend the AbstractStream class to implement your custom streaming logic. This involves defining the stream processing
steps within the createTopology method.

```java
package com.example.streams;

import ch.cern.nile.common.streams.AbstractStream;

import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.KStream;

public class MyCustomStream extends AbstractStream {

    public MyCustomStream(String sourceTopic, String sinkTopic) {
        super(sourceTopic, sinkTopic);
    }

    @Override
    public void createTopology(StreamsBuilder builder) {
        // Define your custom stream processing logic
        builder.stream(sourceTopic, Consumed.with(Serdes.String(), new JsonSerde()))
                .filter(StreamUtils::filterRecord)
                .processValues(new InjectOffsetProcessorSupplier(), InjectOffsetProcessorSupplier.getSTORE_NAME())
                .mapValues(value -> { /* your transformation logic */ })
                .filter(StreamUtils::filterNull).to(sinkTopic);
    }


    @Override
    public Map<String, Object> enrichCustomFunction(Map<String, Object> map, JsonObject value) {
        // Optional: Define your custom enrichment logic
        // In this example, we are adding usage of the schema injector
        return SchemaInjector.inject(map);
    }
}
```
## Documentation

Javadoc documentation for Nile Common can be found [here](https://nile-common.docs.cern.ch).

## Support & Contact

For support, questions, or feedback, please contact [Nile Support](mailto:nile-support@cern.ch).