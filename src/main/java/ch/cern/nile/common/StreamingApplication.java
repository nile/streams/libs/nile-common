package ch.cern.nile.common;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

import org.apache.kafka.streams.errors.StreamsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import ch.cern.nile.common.clients.KafkaStreamsClient;
import ch.cern.nile.common.configuration.PropertiesCheck;
import ch.cern.nile.common.configuration.StreamType;
import ch.cern.nile.common.configuration.properties.CommonProperties;
import ch.cern.nile.common.streams.Streaming;

/**
 * {@link StreamingApplication} is the entry point for initializing and starting a Kafka Streams application.
 * This class provides the main method to load configuration properties, perform the necessary validations,
 * and bootstrap the streaming process using a specified streaming implementation.
 */
public final class StreamingApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(StreamingApplication.class);

    private static final int MIN_ARGS_LENGTH = 1;

    private StreamingApplication() {
    }

    /**
     * The main method for the StreamingApplication, serving as the entry point of the application.
     * It loads configuration properties from a provided file path, validates these properties,
     * and initializes the streaming process using a dynamically loaded Streaming implementation.
     *
     * @param args command-line arguments, expecting the path to the properties file as the first argument
     * @throws IllegalArgumentException if the properties file path is not provided
     * @throws StreamsException         if there are issues with loading the properties file,
     *                                  validating properties, or starting the streaming process
     */
    @SuppressFBWarnings(value = "PATH_TRAVERSAL_IN", justification = "This method is only used internally")
    public static void main(final String[] args) {
        if (args.length < MIN_ARGS_LENGTH) {
            throw new IllegalArgumentException("Properties file not passed");
        }

        final String configPath = args[0];
        final Properties configs = new Properties();
        try {
            configs.load(Files.newInputStream(Paths.get(configPath)));
        } catch (IOException e) {
            final String message = "Error while loading the properties file";
            LOGGER.error(message, e);
            throw new StreamsException(message, e);
        }

        final StreamType sType = StreamType.valueOf(configs.getProperty(CommonProperties.STREAM_TYPE.getValue(), null));

        PropertiesCheck.validateProperties(configs, sType);

        final KafkaStreamsClient client = new KafkaStreamsClient();
        client.configure(configs);

        try {
            final Class<?> clazz = Class.forName(configs.getProperty(CommonProperties.STREAM_CLASS.getValue()));
            final Streaming streaming;
            streaming = (Streaming) clazz.getDeclaredConstructor().newInstance();
            streaming.configure(configs);
            streaming.stream(client);
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | ClassCastException
                 | InvocationTargetException | NoSuchMethodException e) {
            final String message = "Error while starting the stream";
            LOGGER.error(message, e);
            throw new StreamsException(message, e);
        }
    }

}
