package ch.cern.nile.common.clients;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

import org.apache.kafka.common.config.SaslConfigs;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.errors.DefaultProductionExceptionHandler;
import org.apache.kafka.streams.errors.LogAndContinueExceptionHandler;
import org.apache.kafka.streams.errors.StreamsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.cern.nile.common.configuration.Configure;
import ch.cern.nile.common.configuration.properties.ClientProperties;
import ch.cern.nile.common.exceptions.ReverseDnsLookupException;
import ch.cern.nile.common.json.JsonSerde;

/**
 * A client for creating and configuring KafkaStreams instances.
 */
public class KafkaStreamsClient implements Configure {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaStreamsClient.class);

    private static final String SECURITY_PROTOCOL = "SASL_SSL";
    private static final String SASL_MECHANISM = "GSSAPI";
    private static final String SASL_KERBEROS_SERVICE_NAME = "kafka";

    private static final String TEST_CLUSTER_NAME = "test";

    private Properties properties;

    /**
     * Configures the KafkaStreams instance using the provided properties. This method sets up various
     * configuration options such as application ID, client ID, bootstrap servers, security protocols,
     * and serialization/deserialization settings based on the properties provided.
     *
     * @param props the properties to be used for the configuration. Expected properties include
     *              client ID, Kafka cluster information, and security settings.
     */
    @Override
    public void configure(final Properties props) {
        final String clientId = props.getProperty(ClientProperties.CLIENT_ID.getValue());
        properties = new Properties();
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, clientId);
        properties.put(StreamsConfig.CLIENT_ID_CONFIG, clientId);

        final String kafkaCluster = props.getProperty(ClientProperties.KAFKA_CLUSTER.getValue());

        if (TEST_CLUSTER_NAME.equals(kafkaCluster)) {
            properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, props.getProperty("bootstrap.servers"));
        } else {
            properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, this.reverseDnsLookup(kafkaCluster));
            properties.put(StreamsConfig.SECURITY_PROTOCOL_CONFIG, SECURITY_PROTOCOL);
            properties.put(SaslConfigs.SASL_MECHANISM, SASL_MECHANISM);
            properties.put(SaslConfigs.SASL_KERBEROS_SERVICE_NAME, SASL_KERBEROS_SERVICE_NAME);
            properties.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG,
                    props.getProperty(ClientProperties.TRUSTSTORE_LOCATION.getValue()));
        }

        properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, JsonSerde.class.getName());
        properties.put(StreamsConfig.DEFAULT_DESERIALIZATION_EXCEPTION_HANDLER_CLASS_CONFIG,
                LogAndContinueExceptionHandler.class.getName());
        properties.put(StreamsConfig.DEFAULT_PRODUCTION_EXCEPTION_HANDLER_CLASS_CONFIG,
                DefaultProductionExceptionHandler.class.getName());
    }

    /**
     * Creates a KafkaStreams instance using the provided topology.
     *
     * @param topology the topology to be used for the KafkaStreams instance
     * @return a configured KafkaStreams instance
     */
    public KafkaStreams create(final Topology topology) {
        try {
            return new KafkaStreams(topology, properties);
        } catch (StreamsException e) {
            final String message =
                    "Failed to create KafkaStreams instance with properties: " + properties;
            LOGGER.error(message, e);
            throw new StreamsException(message, e);
        }
    }

    /**
     * Resolves the provided Kafka cluster domain to a comma-separated list of hostnames with port 9093.
     *
     * @param kafkaCluster the domain of the Kafka cluster
     * @return a comma-separated list of hostnames with port 9093
     */
    private String reverseDnsLookup(final String kafkaCluster) {
        try {
            return performDnsLookup(kafkaCluster);
        } catch (UnknownHostException e) {
            final String message = "Failed to perform reverse DNS lookup for the Kafka cluster: " + kafkaCluster;
            LOGGER.error(message, e);
            throw new ReverseDnsLookupException(message, e);
        }
    }

    /**
     * Resolves the provided Kafka cluster domain to a comma-separated list of hostnames with port 9093.
     * This method performs a reverse DNS lookup and is used internally for setting up Kafka connections.
     *
     * @param kafkaCluster the domain of the Kafka cluster
     * @return a comma-separated list of hostnames with port 9093
     * @throws ReverseDnsLookupException if the hostname resolution fails
     */
    protected String performDnsLookup(final String kafkaCluster) throws UnknownHostException {
        final StringBuilder stringBuilder = new StringBuilder();
        final InetAddress[] address = InetAddress.getAllByName(kafkaCluster);
        for (final InetAddress host : address) {
            final String hostName = InetAddress.getByName(host.getHostAddress()).getHostName();
            stringBuilder.append(hostName).append(":9093,");
        }
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        return stringBuilder.toString();
    }
}
