package ch.cern.nile.common.configuration;

import java.util.Properties;

/**
 * Interface for classes that can be configured with a Properties object.
 */
public interface Configure {

    /**
     * Configure this class.
     *
     * @param properties the properties to use for configuration
     */
    void configure(Properties properties);
}
