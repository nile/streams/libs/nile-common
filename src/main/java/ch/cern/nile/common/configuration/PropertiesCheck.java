package ch.cern.nile.common.configuration;

import java.util.Objects;
import java.util.Properties;
import java.util.Set;

import ch.cern.nile.common.configuration.properties.ClientProperties;
import ch.cern.nile.common.configuration.properties.CommonProperties;
import ch.cern.nile.common.configuration.properties.DecodingProperties;
import ch.cern.nile.common.configuration.properties.EnrichmentProperties;
import ch.cern.nile.common.configuration.properties.PropertyEnum;
import ch.cern.nile.common.configuration.properties.RoutingProperties;
import ch.cern.nile.common.configuration.properties.SchemaTransformationProperties;
import ch.cern.nile.common.exceptions.MissingPropertyException;
import ch.cern.nile.common.exceptions.UnknownStreamTypeException;

/**
 * A utility class to validate properties based on the type of stream.
 */
public final class PropertiesCheck {

    private static final Set<String> CLIENT_PROPERTIES = PropertyEnum.getValues(ClientProperties.class);
    private static final Set<String> COMMON_PROPERTIES = PropertyEnum.getValues(CommonProperties.class);
    private static final Set<String> DECODING_PROPERTIES = PropertyEnum.getValues(DecodingProperties.class);
    private static final Set<String> ROUTING_PROPERTIES = PropertyEnum.getValues(RoutingProperties.class);
    private static final Set<String> ENRICHMENT_PROPERTIES = PropertyEnum.getValues(EnrichmentProperties.class);
    private static final Set<String> SCHEMA_TRANSFORMATION_PROPERTIES = PropertyEnum.getValues(
            SchemaTransformationProperties.class);

    private PropertiesCheck() {
    }

    /**
     * Validates the properties file based on the type of stream (DECODING, ROUTING, or ENRICHMENT).
     * This method checks if all required properties for the specified stream type are present in the
     * properties object, throwing exceptions if any are missing.
     *
     * @param properties the properties already loaded from file into java.util.Properties object
     * @param streamType the type of stream defined in the properties file
     * @throws MissingPropertyException   if a required property is missing from the properties object
     * @throws UnknownStreamTypeException if the stream type is unknown
     */
    public static void validateProperties(final Properties properties, final StreamType streamType) {
        Objects.requireNonNull(properties, "Properties object cannot be null");
        Objects.requireNonNull(streamType, "Properties file is missing stream.type property");

        validateRequiredProperties(properties, CLIENT_PROPERTIES);
        validateRequiredProperties(properties, COMMON_PROPERTIES);

        switch (streamType) {
            case DECODING:
                validateRequiredProperties(properties, DECODING_PROPERTIES);
                break;
            case ROUTING:
                validateRequiredProperties(properties, ROUTING_PROPERTIES);
                break;
            case ENRICHMENT:
                validateRequiredProperties(properties, ENRICHMENT_PROPERTIES);
                break;
            case SCHEMA_TRANSFORMATION:
                validateRequiredProperties(properties, SCHEMA_TRANSFORMATION_PROPERTIES);
                break;
            default:
                // Cannot happen as the stream type is validated before this switch statement.
                throw new UnknownStreamTypeException(String.format("Stream type unknown: %s.", streamType));
        }
    }

    /**
     * Validates the required properties within the given properties object.
     *
     * @param props        the properties object to check for required properties.
     * @param propsToCheck the set of required property keys.
     * @throws MissingPropertyException if a required property is missing from the properties object.
     */
    private static void validateRequiredProperties(final Properties props, final Set<String> propsToCheck) {
        Objects.requireNonNull(props, "Properties object cannot be null");
        Objects.requireNonNull(propsToCheck, "Properties to check cannot be null");

        for (final String prop : propsToCheck) {
            if (!props.containsKey(prop)) {
                throw new MissingPropertyException(String.format("Properties file is missing: %s property.", prop));
            }
        }
    }

}
