package ch.cern.nile.common.configuration;

/**
 * Enum representing the types of streams supported by the application.
 */
public enum StreamType {

    ROUTING,
    DECODING,
    ENRICHMENT,
    SCHEMA_TRANSFORMATION

}
