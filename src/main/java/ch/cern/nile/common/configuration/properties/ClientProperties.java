package ch.cern.nile.common.configuration.properties;

import lombok.Getter;

/**
 * Enum representing various properties specific to clients in the application.
 */
@Getter
public enum ClientProperties implements PropertyEnum {
    SOURCE_TOPIC("source.topic"),
    KAFKA_CLUSTER("kafka.cluster"),
    CLIENT_ID("client.id"),
    TRUSTSTORE_LOCATION("truststore.location");

    private final String value;

    ClientProperties(final String value) {
        this.value = value;
    }
}
