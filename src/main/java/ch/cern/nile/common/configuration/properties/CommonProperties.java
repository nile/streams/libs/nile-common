package ch.cern.nile.common.configuration.properties;

import lombok.Getter;

/**
 * Enum representing common properties used throughout the application.
 */
@Getter
public enum CommonProperties implements PropertyEnum {
    STREAM_TYPE("stream.type"),
    STREAM_CLASS("stream.class");

    private final String value;

    CommonProperties(final String value) {
        this.value = value;
    }
}
