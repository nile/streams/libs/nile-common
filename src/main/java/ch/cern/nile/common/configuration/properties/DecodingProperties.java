package ch.cern.nile.common.configuration.properties;

import lombok.Getter;

/**
 * Enum representing properties related to the decoding process in the application.
 */
@Getter
public enum DecodingProperties implements PropertyEnum {
    SINK_TOPIC("sink.topic");

    private final String value;

    DecodingProperties(final String value) {
        this.value = value;
    }
}
