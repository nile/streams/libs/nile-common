package ch.cern.nile.common.configuration.properties;

import lombok.Getter;

/**
 * Enum representing properties related to data enrichment in the application.
 */
@Getter
public enum EnrichmentProperties implements PropertyEnum {
    ENRICHMENT_CONFIG_PATH("enrichment.config.path"),
    SINK_TOPIC("sink.topic");

    private final String value;

    EnrichmentProperties(final String value) {
        this.value = value;
    }
}
