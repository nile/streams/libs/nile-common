package ch.cern.nile.common.configuration.properties;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Interface representing an enumeration of property keys. Enums implementing this interface
 * can be used to define sets of configuration properties.
 * Each enum constant in an implementing class represents a specific property key.
 * <p>
 * This interface provides a method to retrieve the string values associated with each enum constant.
 * <p>
 * Implementing enums should define a private final field to store the property key and use the
 * {@link lombok.Getter} annotation to automatically generate the required getter method.
 */
public interface PropertyEnum {

    /**
     * Retrieves the string value associated with this enum constant.
     * It's suggested to use {@link lombok.Getter} to generate this method.
     *
     * @return the string value associated with this enum constant
     */
    String getValue();

    /**
     * Retrieves the string values associated with each enum constant of a given enum type that
     * implements PropertyEnum.
     *
     * @param enumClass the class object of the enum type.
     * @param <E>       the type of the enum class that implements PropertyEnum
     * @return a set containing the string values of all the enum constants in the specified enum
     */
    static <E extends Enum<E> & PropertyEnum> Set<String> getValues(final Class<E> enumClass) {
        return Arrays.stream(enumClass.getEnumConstants()).map(PropertyEnum::getValue).collect(Collectors.toSet());
    }
}
