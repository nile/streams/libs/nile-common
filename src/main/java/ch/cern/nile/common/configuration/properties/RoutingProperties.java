package ch.cern.nile.common.configuration.properties;

import lombok.Getter;

/**
 * Enum representing properties related to message routing within the application.
 */
@Getter
public enum RoutingProperties implements PropertyEnum {
    ROUTING_CONFIG_PATH("routing.config.path"),
    DLQ_TOPIC("dlq.topic");

    private final String value;

    RoutingProperties(final String value) {
        this.value = value;
    }
}
