package ch.cern.nile.common.configuration.properties;

import lombok.Getter;

/**
 * Enum representing properties related to schema transformation in the application.
 */
@Getter
public enum SchemaTransformationProperties implements PropertyEnum {
    SCHEMA_REGISTRY_URL("schema.registry.url"),
    SINK_TOPIC("sink.topic");

    private final String value;

    SchemaTransformationProperties(final String value) {
        this.value = value;
    }
}
