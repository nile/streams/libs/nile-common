package ch.cern.nile.common.exceptions;

import java.io.Serial;

public class DecodingException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = 1L;

    public DecodingException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public DecodingException(final String message) {
        super(message);
    }

}
