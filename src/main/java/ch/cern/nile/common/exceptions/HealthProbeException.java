package ch.cern.nile.common.exceptions;

import java.io.Serial;

public class HealthProbeException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = 1L;

    public HealthProbeException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
