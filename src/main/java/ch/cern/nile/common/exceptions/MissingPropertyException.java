package ch.cern.nile.common.exceptions;

import java.io.Serial;

public class MissingPropertyException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = 1L;

    public MissingPropertyException(final String message) {
        super(message);
    }

    public MissingPropertyException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
