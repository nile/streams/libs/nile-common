package ch.cern.nile.common.exceptions;

import java.io.Serial;

public class UnknownStreamTypeException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = 1L;

    public UnknownStreamTypeException(final String message) {
        super(message);
    }

}
