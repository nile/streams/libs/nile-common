package ch.cern.nile.common.json;

import java.nio.charset.StandardCharsets;
import java.util.Map;

import com.google.gson.Gson;

import org.apache.kafka.common.serialization.Deserializer;

/**
 * A deserializer for JSON POJOs using Gson. This class implements the Deserializer interface
 * from Apache Kafka and provides a mechanism to convert JSON byte data back into Java objects (POJOs)
 * of a specified type.
 *
 * @param <T> The type of the POJO to be deserialized.
 */
public class JsonPojoDeserializer<T> implements Deserializer<T> {

    private static final Gson GSON = new Gson();

    /**
     * The class type for the deserialization.
     */
    private Class<T> tClass;

    /**
     * Constructs a new JsonPojoDeserializer with the given class type for deserialization.
     *
     * @param clazz the class type for the deserialization
     */
    JsonPojoDeserializer(final Class<T> clazz) {
        this.tClass = clazz;
    }

    /**
     * Configures this class with the given properties.
     *
     * @param props the properties from the consumer configuration
     * @param isKey is ignored in this implementation
     */
    @Override
    @SuppressWarnings("unchecked")
    public void configure(final Map<String, ?> props, final boolean isKey) {
        if (tClass == null) {
            tClass = (Class<T>) props.get("JsonPOJOClass");
        }
    }

    /**
     * Deserializes the provided byte array into an object of type T.
     *
     * @param topic the topic associated with the data
     * @param bytes the byte array to be deserialized
     * @return the deserialized object of type T or null if the byte array is null
     */
    @Override
    public T deserialize(final String topic, final byte[] bytes) {
        T deserializedData = null;
        if (bytes != null) {
            deserializedData = GSON.fromJson(new String(bytes, StandardCharsets.UTF_8), tClass);
        }
        return deserializedData;
    }

    /**
     * Closes this deserializer.
     * This method is required by the Serializer interface but does nothing in this implementation.
     */
    @Override
    public void close() {
        // Nothing to do
    }

}
