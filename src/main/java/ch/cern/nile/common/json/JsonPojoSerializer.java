package ch.cern.nile.common.json;

import java.nio.charset.StandardCharsets;
import java.util.Map;

import com.google.gson.Gson;

import org.apache.kafka.common.serialization.Serializer;

/**
 * A serializer for JSON POJOs using Gson. This class implements the Serializer interface
 * from Apache Kafka and provides a mechanism to convert Java objects (POJOs) of a specified type
 * into JSON byte data.
 *
 * @param <T> The type of the POJO to be serialized.
 */
public class JsonPojoSerializer<T> implements Serializer<T> {

    private static final Gson GSON = new Gson();

    /**
     * Configures this serializer. This method is part of the Serializer interface but is not used
     * in this implementation.
     *
     * @param props is ignored in this implementation
     * @param isKey is ignored in this implementation
     */
    @Override
    public void configure(final Map<String, ?> props, final boolean isKey) {
        // Nothing to do
    }

    /**
     * Serializes the provided data into a JSON string using Gson and converts it to a byte array.
     *
     * @param topic the topic associated with the data. This is not used in the serialization process
     * @param data  the POJO to be serialized
     * @return the serialized data as bytes, or null if the data is null
     */
    @Override
    public byte[] serialize(final String topic, final T data) {
        byte[] serializedData = null;
        if (data != null) {
            serializedData = GSON.toJson(data).getBytes(StandardCharsets.UTF_8);
        }
        return serializedData;
    }

    /**
     * Closes this serializer.
     * This method is required by the Serializer interface but does nothing in this implementation.
     */
    @Override
    public void close() {
        // Nothing to do
    }

}
