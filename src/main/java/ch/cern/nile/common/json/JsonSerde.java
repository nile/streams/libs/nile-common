package ch.cern.nile.common.json;

import java.util.Map;

import com.google.gson.JsonObject;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

/**
 * A Serde (Serializer/Deserializer) implementation for JSON objects using Gson. This class
 * provides both serialization and deserialization capabilities for Kafka streams to handle
 * JSON objects represented by the {@link JsonObject} class.
 */
public class JsonSerde implements Serde<JsonObject> {

    private final JsonPojoSerializer<JsonObject> jsonSerializer = new JsonPojoSerializer<>();
    private final JsonPojoDeserializer<JsonObject> jsonDeserializer = new JsonPojoDeserializer<>(JsonObject.class);

    /**
     * Configures this Serde with the given properties. This method configures both the internal
     * serializer and deserializer with the provided configuration settings.
     *
     * @param configs the properties from the consumer or producer configuration
     * @param isKey   indicates whether this Serde is being used for key serialization/deserialization
     *                This parameter is ignored in this implementation
     */
    @Override
    public void configure(final Map<String, ?> configs, final boolean isKey) {
        jsonSerializer.configure(configs, isKey);
        jsonDeserializer.configure(configs, isKey);
    }

    /**
     * Closes this Serde. This method closes both the internal serializer and deserializer.
     */
    @Override
    public void close() {
        jsonSerializer.close();
        jsonDeserializer.close();
    }

    /**
     * Returns the serializer component of this Serde.
     *
     * @return The {@link JsonPojoSerializer} for serializing JSON objects
     */
    @Override
    public Serializer<JsonObject> serializer() {
        return jsonSerializer;
    }

    /**
     * Returns the deserializer component of this Serde.
     *
     * @return The {@link JsonPojoDeserializer} for deserializing JSON objects
     */
    @Override
    public Deserializer<JsonObject> deserializer() {
        return jsonDeserializer;
    }
}
