package ch.cern.nile.common.models;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Model representing an application with its name and associated topic.
 * Primarily used in serialization and deserialization processes.
 */
@NoArgsConstructor
@Getter
@Setter
@ToString
@SuppressFBWarnings(value = "EI_EXPOSE_REP",
        justification = "This is a model class used for serialization and deserialization")
public class Application {

    private String name;
    private Topic topic;

}
