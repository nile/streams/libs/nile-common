package ch.cern.nile.common.models;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Model representing a topic, identified by its name.
 * Used in serialization and deserialization processes.
 */
@NoArgsConstructor
@Getter
@Setter
@ToString
@SuppressFBWarnings(value = "EI_EXPOSE_REP",
        justification = "This is a model class used for serialization and deserialization")
public class Topic {

    private String name;

}
