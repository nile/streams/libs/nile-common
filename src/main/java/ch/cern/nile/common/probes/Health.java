package ch.cern.nile.common.probes;

import java.io.IOException;
import java.net.InetSocketAddress;

import com.sun.net.httpserver.HttpServer;

import org.apache.kafka.streams.KafkaStreams;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import ch.cern.nile.common.exceptions.HealthProbeException;

/**
 * A simple HTTP server that responds to health checks at the "/health" endpoint.
 * It returns a 200-OK response if the KafkaStreams instance is running, or a 500-Internal Server Error
 * if it is not running. By default, the server listens on port 8899.
 */
public class Health {

    private static final int OK_RESPONSE = 200;
    private static final int ERROR_RESPONSE = 500;
    private static final int PORT = 8899;

    private final KafkaStreams streams;
    private HttpServer server;
    private final HttpServerFactory httpServerFactory;

    /**
     * Creates a new Health instance that will respond to health checks on port 8899.
     * Health checks determine the running state of the provided KafkaStreams instance.
     *
     * @param streams the KafkaStreams instance to check the state of
     */
    public Health(final KafkaStreams streams) {
        this(streams, new DefaultHttpServerFactory());
    }

    /**
     * Creates a new Health instance that will respond to health checks on port 8899, using the provided
     * HttpServerFactory. This constructor is useful for testing. Health checks determine the running state
     * of the provided KafkaStreams instance.
     *
     * @param streams           the KafkaStreams instance to check the state of
     * @param httpServerFactory the factory to use to create the HttpServer instance
     */
    @SuppressFBWarnings(value = "EI_EXPOSE_REP2",
            justification = "This is an internal class and the HttpServerFactory is not exposed to the outside")
    public Health(final KafkaStreams streams, final HttpServerFactory httpServerFactory) {
        this.streams = streams;
        this.httpServerFactory = httpServerFactory;
    }

    /**
     * Starts the Health HTTP server. The server listens for health check requests and responds
     * based on the state of the KafkaStreams instance.
     */
    public void start() {
        try {
            server = httpServerFactory.createHttpServer(new InetSocketAddress(PORT), 0);
        } catch (IOException ex) {
            throw new HealthProbeException("Failed to create HTTP server", ex);
        }
        server.createContext("/health", exchange -> {
            final int responseCode = streams.state().isRunningOrRebalancing() ? OK_RESPONSE : ERROR_RESPONSE;
            exchange.sendResponseHeaders(responseCode, 0);
            exchange.close();
        });
        server.start();
    }

    /**
     * Stops the Health HTTP server, terminating the health check responses.
     */
    public void stop() {
        server.stop(0);
    }

    /**
     * The default HttpServerFactory implementation used to create HttpServer instances.
     */
    private static final class DefaultHttpServerFactory implements HttpServerFactory {
        @Override
        public HttpServer createHttpServer(final InetSocketAddress address, final int backlog) throws IOException {
            return HttpServer.create(address, backlog);
        }
    }
}
