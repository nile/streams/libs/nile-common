package ch.cern.nile.common.probes;

import java.io.IOException;
import java.net.InetSocketAddress;

import com.sun.net.httpserver.HttpServer;

/**
 * Factory for creating HttpServer instances. This interface is used to allow mocking of HttpServer
 * in tests and to provide flexibility in the instantiation of HttpServer, facilitating dependency
 * injection and customization.
 */
public interface HttpServerFactory {

    /**
     * Creates a new HttpServer instance bound to the specified address with the given backlog.
     *
     * @param address the address to bind the server to
     * @param backlog the maximum number of pending connections
     * @return the created HttpServer instance
     * @throws IOException if an I/O error occurs when creating the HttpServer
     */
    HttpServer createHttpServer(InetSocketAddress address, int backlog) throws IOException;
}
