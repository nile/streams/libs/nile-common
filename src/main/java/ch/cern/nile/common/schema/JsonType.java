package ch.cern.nile.common.schema;

import java.util.Date;

import lombok.Getter;

/**
 * Enum mapping Java classes to their corresponding JSON types for Connect schema(s).
 * This enum provides a convenient way to determine the JSON type representation
 * of various Java data types.
 */
@Getter
enum JsonType {
    BYTE(Byte.class, "int8"),
    SHORT(Short.class, "int16"),
    INTEGER(Integer.class, "int32"),
    LONG(Long.class, "int64"),
    FLOAT(Float.class, "float"),
    DOUBLE(Double.class, "double"),
    BOOLEAN(Boolean.class, "boolean"),
    STRING(String.class, "string"),
    DATE(Date.class, "int64"),
    BYTE_ARRAY(byte[].class, "bytes");

    private final Class<?> clazz;
    private final String type;

    /**
     * Constructs a new JsonType enum constant.
     *
     * @param clazz the Java class associated with this JSON type
     * @param type  the string representation of the JSON type
     */
    JsonType(final Class<?> clazz, final String type) {
        this.clazz = clazz;
        this.type = type;
    }

    /**
     * Returns the JsonType corresponding to the given Java class.
     * Throws an IllegalArgumentException if the class is not supported.
     *
     * @param clazz the Java class to find the corresponding JsonType for
     * @return the JsonType corresponding to the given class
     * @throws IllegalArgumentException if the class is not supported
     */
    static JsonType fromClass(final Class<?> clazz) {
        for (final JsonType jsonType : values()) {
            if (jsonType.getClazz().equals(clazz)) {
                return jsonType;
            }
        }
        throw new IllegalArgumentException("Unsupported class: " + clazz.getSimpleName());
    }
}
