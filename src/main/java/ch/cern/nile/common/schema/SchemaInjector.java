package ch.cern.nile.common.schema;

import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import com.google.gson.JsonPrimitive;

/**
 * Utility class for injecting Connect schemas into given data. The class provides static methods
 * to generate a schema based on the data types present in a map and inject this schema into the data.
 */
public final class SchemaInjector {

    private SchemaInjector() {
    }

    /**
     * Injects a Connect schema into the given data. The method generates a schema based on the data types
     * in the input map and returns a new map containing both the original data and the generated schema.
     *
     * @param data the data to inject the schema into
     * @return a new map containing the original data and the injected schema
     */
    public static Map<String, Object> inject(final Map<String, Object> data) {
        final Map<String, Object> dataCopy = new HashMap<>(data);
        final Map<String, Object> schemaMap = generateSchemaMap(dataCopy);

        final Map<String, Object> result = new HashMap<>();
        result.put("schema", schemaMap);
        result.put("payload", dataCopy);

        return result;
    }

    private static Map<String, Object> generateSchemaMap(final Map<String, Object> data) {
        final Map<String, Object> schemaMap = new HashMap<>();
        schemaMap.put("type", "struct");
        schemaMap.put("fields", generateFieldMaps(data));

        return schemaMap;
    }

    private static Iterable<Map<String, Object>> generateFieldMaps(final Map<String, Object> data) {
        return data.entrySet().stream().map(SchemaInjector::generateFieldMap).collect(Collectors.toList());
    }

    private static Map<String, Object> generateFieldMap(final Map.Entry<String, Object> entry) {
        final Map<String, Object> fieldMap = new HashMap<>();
        final String key = entry.getKey();
        Object value = entry.getValue();

        validateValue(value);
        if (value instanceof JsonPrimitive) {
            // TODO(#): test this quick bugfix further
            value = ((JsonPrimitive) value).getAsString();
        }

        final JsonType type = JsonType.fromClass(value.getClass());

        fieldMap.put("field", key);
        fieldMap.put("type", type.getType());
        fieldMap.put("optional", !key.toLowerCase(Locale.ENGLISH).contains("timestamp"));

        addTimestampAndDateFields(fieldMap, key, type);

        return fieldMap;
    }

    private static void validateValue(final Object value) {
        if (value == null) {
            throw new IllegalArgumentException("Null values are not allowed in the data map.");
        }
    }

    private static void addTimestampAndDateFields(final Map<String, Object> fieldMap, final String key,
                                                  final JsonType type) {
        final boolean isTimestampField = key.toLowerCase(Locale.ENGLISH).contains("timestamp");
        final boolean isDateType = type.getClazz().equals(Date.class);

        if (isTimestampField) {
            fieldMap.put("name", "org.apache.kafka.connect.data.Timestamp");
            fieldMap.put("version", 1);
        } else if (isDateType) {
            fieldMap.put("name", "org.apache.kafka.connect.data.Date");
            fieldMap.put("version", 1);
        }
    }
}
