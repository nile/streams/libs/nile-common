package ch.cern.nile.common.streams;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Produced;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.confluent.kafka.serializers.json.KafkaJsonSchemaSerializerConfig;
import io.confluent.kafka.streams.serdes.json.KafkaJsonSchemaSerde;

/**
 * Abstract class for creating Kafka Streams topologies that transform JSON messages to POJOs using a JSON schema.
 *
 * @param <T> The type of the POJO to transform the JSON messages to
 */
public abstract class AbstractJsonSchemaTransformerStream<T> extends AbstractStream {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractJsonSchemaTransformerStream.class);

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final Class<T> clazz;

    /**
     * Constructor for the AbstractJsonSchemaTransformerStream.
     *
     * @param clazz The class of the POJO to transform the JSON messages to
     */
    public AbstractJsonSchemaTransformerStream(final Class<T> clazz) {
        this.clazz = clazz;
    }

    /**
     * Creates the Kafka Streams topology.
     *
     * @param builder The StreamsBuilder to use
     */
    @Override
    public void createTopology(final StreamsBuilder builder) {
        final Map<String, Object> serdeConfig = new HashMap<>();
        serdeConfig.put(KafkaJsonSchemaSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, getSchemaRegistryUrl());

        try (KafkaJsonSchemaSerde<T> jsonSchemaSerde = new KafkaJsonSchemaSerde<>(clazz)) {
            jsonSchemaSerde.configure(serdeConfig, false);

            builder.stream(getSourceTopic(), Consumed.with(Serdes.String(), Serdes.String()))
                    .mapValues(this::convertToPojo)
                    .filter(StreamUtils::filterNull)
                    .to(getSinkTopic(), Produced.with(Serdes.String(), jsonSchemaSerde));
        }
    }

    private T convertToPojo(final String jsonString) {
        T result = null;
        try {
            result = objectMapper.readValue(jsonString, clazz);
        } catch (JsonProcessingException e) {
            LOGGER.error("Failed to convert JSON to POJO", e);
            logStreamsException(e);
        }
        return result;
    }
}
