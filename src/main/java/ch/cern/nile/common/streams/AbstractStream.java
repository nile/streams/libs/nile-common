package ch.cern.nile.common.streams;

import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.errors.StreamsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import ch.cern.nile.common.clients.KafkaStreamsClient;
import ch.cern.nile.common.configuration.properties.ClientProperties;
import ch.cern.nile.common.configuration.properties.DecodingProperties;
import ch.cern.nile.common.configuration.properties.SchemaTransformationProperties;
import ch.cern.nile.common.probes.Health;

/**
 * AbstractStream is an abstract class implementing the {@link Streaming} interface, providing a framework
 * for building and managing Kafka Streams applications. It encapsulates common functionality such
 * as configuring the stream, handling its lifecycle, and managing health checks. Implementations of
 * this class should provide the specific logic for creating the Kafka Streams topology by overriding
 * the createTopology method.
 * <p>
 * Usage:
 * Subclasses should implement the {@link AbstractStream#createTopology(StreamsBuilder) CreateTopology}
 * method to define their specific processing topology.
 * They can then use this abstract class to handle common streaming functionalities
 * such as starting, stopping, and monitoring the Kafka Streams application.
 */
public abstract class AbstractStream implements Streaming {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractStream.class);

    @Getter(AccessLevel.PROTECTED)
    private String sourceTopic;

    @Getter(AccessLevel.PROTECTED)
    private String sinkTopic;

    @Getter(AccessLevel.PROTECTED)
    private String schemaRegistryUrl;

    @Setter(AccessLevel.PROTECTED)
    private long lastReadOffset = -2;

    private Properties properties;
    private KafkaStreams streams;
    private Health health;
    private CountDownLatch latch;

    /**
     * Configures the Kafka Streams application with provided settings.
     *
     * @param configs the configuration settings for the Kafka Streams application
     */
    @Override
    @SuppressFBWarnings(value = "EI_EXPOSE_REP2",
            justification = "This is an internal class and the properties are not exposed to the outside")
    public void configure(final Properties configs) {
        this.properties = configs;
    }

    /**
     * Starts the Kafka Streams application using the provided KafkaStreamsClient and initializes
     * its lifecycle management, including graceful shutdown. This method also adds a shutdown hook
     * to the JVM and terminates the JVM upon completion of the stream.
     *
     * @param kafkaStreamsClient the client used to create and manage the Kafka Streams instance
     */
    @Override
    @SuppressWarnings("PMD.DoNotTerminateVM")
    @SuppressFBWarnings(value = "DM_EXIT", justification = "This is a Kafka Streams application")
    public void stream(final KafkaStreamsClient kafkaStreamsClient) {
        init(kafkaStreamsClient);
        Runtime.getRuntime().addShutdownHook(new Thread(this::shutDown, "streams-shutdown-hook"));
        start();
        System.exit(0);
    }

    /**
     * Implement this method to define the topology of the Kafka Streams application.
     *
     * @param builder the {@link StreamsBuilder} to use to create the topology
     */
    public abstract void createTopology(StreamsBuilder builder);

    /**
     * Use this method to log any exceptions that occur while streaming.
     *
     * @param exception the exception to log
     */
    protected void logStreamsException(final Exception exception) {
        if (LOGGER.isWarnEnabled()) {
            LOGGER.warn(
                    String.format("Error reading from topic %s. Last read offset: %s", sourceTopic, lastReadOffset),
                    exception);
        }
        if (streams != null & LOGGER.isInfoEnabled()) {
            LOGGER.info("Current state of streams: {}", streams.state());
        }
    }

    private void init(final KafkaStreamsClient kafkaStreamsClient) {
        final StreamsBuilder builder = new StreamsBuilder();
        sourceTopic = properties.getProperty(ClientProperties.SOURCE_TOPIC.getValue());
        sinkTopic = properties.getProperty(DecodingProperties.SINK_TOPIC.getValue());
        schemaRegistryUrl = properties.getProperty(SchemaTransformationProperties.SCHEMA_REGISTRY_URL.getValue());
        createTopology(builder);
        final Topology topology = builder.build();
        streams = kafkaStreamsClient.create(topology);
        health = new Health(streams);
        latch = new CountDownLatch(1);
    }

    private void start() {
        LOGGER.info("Starting streams...");
        streams.start();
        health.start();
        try {
            latch.await();
        } catch (InterruptedException e) {
            LOGGER.error("Error while waiting for latch", e);
            throw new StreamsException("Error while waiting for latch", e);
        }

    }

    private void shutDown() {
        LOGGER.info("Shutting down streams...");
        streams.close();
        health.stop();
        latch.countDown();
    }

}
