package ch.cern.nile.common.streams;

import java.time.Instant;
import java.util.List;
import java.util.Map;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.cern.nile.common.exceptions.DecodingException;

/**
 * {@link StreamUtils} is a utility class providing static methods to assist in stream processing.
 */
public final class StreamUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(StreamUtils.class);

    private StreamUtils() {
    }

    /**
     * Adds the most recent timestamp found in the gatewayInfo JsonArray to the provided map.
     * The timestamp is added as an epoch millisecond value under the key "timestamp".
     *
     * @param gatewayInfo the JsonArray containing gateway information, each entry expected to
     *                    have a "time" field with an ISO-8601 formatted timestamp
     * @param map         the map to which the most recent timestamp will be added
     * @throws DecodingException if no valid timestamp is found in the gatewayInfo
     */
    public static void addTimestamp(final JsonArray gatewayInfo, final Map<String, Object> map) {
        final String timeKey = "time";
        Instant timestamp = null;
        for (final JsonElement element : gatewayInfo) {
            if (!element.isJsonObject()) {
                continue;
            }

            final JsonObject entry = element.getAsJsonObject();
            if (!entry.has(timeKey)) {
                continue;
            }

            final Instant currentTimestamp = Instant.parse(entry.get(timeKey).getAsString());
            if (timestamp == null || currentTimestamp.isAfter(timestamp)) {
                timestamp = currentTimestamp;
            }
        }

        if (timestamp == null) {
            if (LOGGER.isWarnEnabled()) {
                LOGGER.warn("No valid {} found in gatewayInfo: {}", timeKey, gatewayInfo);
            }
            timestamp = Instant.now();
        }

        map.put("timestamp", timestamp.toEpochMilli());
    }

    /**
     * Filters out null values.
     *
     * @param ignored ignored parameter (unused in current implementation)
     * @param value   the value to be checked for null
     * @return true if the value is not null, false otherwise
     */
    public static boolean filterNull(final String ignored, final Object value) {
        return value != null;
    }

    /**
     * Filters out empty lists and maps.
     * Returns true if the value is neither an empty list nor an empty map, otherwise false.
     * <p>
     * This method is useful in stream processing scenarios where empty collections (lists or maps) are considered
     * irrelevant or need to be filtered out.
     *
     * @param ignored ignored parameter (unused in current implementation)
     * @param value   the value to be checked, expected to be a List or Map
     * @return true if the value is not an empty list or map, false otherwise
     */
    public static boolean filterEmpty(final String ignored, final Object value) {
        boolean isNotEmpty = true;

        if (value instanceof List) {
            isNotEmpty = !((List<?>) value).isEmpty();
        } else if (value instanceof Map) {
            isNotEmpty = !((Map<?, ?>) value).isEmpty();
        }

        return isNotEmpty;
    }

    /**
     * Filters records based on the presence of required fields in a JsonObject.
     * Returns true if the JsonObject contains all required fields ("applicationID", "applicationName",
     * "deviceName", "devEUI", and "data"), otherwise false.
     *
     * @param ignored ignored parameter (unused in current implementation)
     * @param value   the JsonObject to be checked for required fields
     * @return true if all required fields are present, false otherwise
     */
    public static boolean filterRecord(final String ignored, final JsonObject value) {
        return value != null && value.get("applicationID") != null && value.get("applicationName") != null
                && value.get("deviceName") != null && value.get("devEUI") != null
                && value.get("data") != null;
    }

}
