package ch.cern.nile.common.streams;

import ch.cern.nile.common.clients.KafkaStreamsClient;
import ch.cern.nile.common.configuration.Configure;

/**
 * The Streaming interface defines the essential functions for a streaming application.
 * It extends the Configure interface, allowing for configuration setup. Implementations
 * of this interface are responsible for defining the streaming behavior, including
 * how streams are created and managed to use a KafkaStreamsClient.
 */
public interface Streaming extends Configure {

    /**
     * Initializes and starts the streaming process using the provided KafkaStreamsClient.
     * Implementations should define the setup and execution of the stream, including the
     * creation and management of Kafka Streams instances.
     *
     * @param kafkaStreamsClient the KafkaStreamsClient used to create and manage the stream.
     */
    void stream(KafkaStreamsClient kafkaStreamsClient);

}
