package ch.cern.nile.common.streams.offsets;

import java.util.Collections;
import java.util.Set;

import com.google.gson.JsonObject;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.processor.api.FixedKeyProcessor;
import org.apache.kafka.streams.processor.api.FixedKeyProcessorSupplier;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.StoreBuilder;
import org.apache.kafka.streams.state.Stores;

import lombok.Getter;

/**
 * Supplies a {@link FixedKeyProcessor} for injecting offsets into Kafka records' values.
 * This class is used in the configuration of a Kafka Streams topology,
 * specifically to provide instances of {@link InjectOffsetTransformer}.
 * It also configures the required state store for processing.
 * <p>
 * Usage Example:
 * <pre>{@code
 * StreamsBuilder builder = new StreamsBuilder();
 * builder.stream(getSourceTopic(), Consumed.with(Serdes.String(), new JsonSerde()))
 *        .processValues(new InjectOffsetProcessorSupplier(), InjectOffsetProcessorSupplier.getSTORE_NAME())
 *        .to(sinkTopic);
 * }</pre>
 */
public class InjectOffsetProcessorSupplier implements FixedKeyProcessorSupplier<String, JsonObject, JsonObject> {

    @Getter
    private static final String STORE_NAME = "InjectOffsetStore";

    /**
     * Provides an instance of {@link InjectOffsetTransformer},
     * which is configured to enrich JSON records with their Kafka offsets.
     *
     * @return a new instance of {@link InjectOffsetTransformer}
     */
    @Override
    public FixedKeyProcessor<String, JsonObject, JsonObject> get() {
        return new InjectOffsetTransformer();
    }

    /**
     * Defines and provides the necessary state store for the processor.
     * This store is used internally by the processor if stateful operations are required.
     *
     * @return a set containing the store builder for the state store
     */
    @Override
    public Set<StoreBuilder<?>> stores() {
        final StoreBuilder<KeyValueStore<String, String>> keyValueStoreBuilder =
                Stores.keyValueStoreBuilder(Stores.persistentKeyValueStore(STORE_NAME), Serdes.String(),
                        Serdes.String());
        return Collections.singleton(keyValueStoreBuilder);
    }
}
