package ch.cern.nile.common.streams.offsets;

import com.google.gson.JsonObject;

import org.apache.kafka.streams.processor.api.FixedKeyProcessor;
import org.apache.kafka.streams.processor.api.FixedKeyProcessorContext;
import org.apache.kafka.streams.processor.api.FixedKeyRecord;

/**
 * This class is a Kafka Streams processor that enhances each record by injecting the Kafka offset into the record's
 * JSON payload.
 * It implements the {@link FixedKeyProcessor} interface to process records with a fixed key type of String and a
 * value type of JsonObject.
 * This processor is typically used in a Kafka Streams topology where records require enrichment with their offset
 * for downstream processing or auditing.
 * <p>
 * Usage Example:
 * See usage in {@link InjectOffsetProcessorSupplier} class documentation.
 */
public class InjectOffsetTransformer implements FixedKeyProcessor<String, JsonObject, JsonObject> {

    private static final String OFFSET_PROPERTY_NAME = "offset";

    private FixedKeyProcessorContext<String, JsonObject> context;

    /**
     * Initializes the processor with the processing context, providing access to metadata such as the record's offset.
     *
     * @param fixedKeyProcessorContext the context that provides metadata and the capability to forward modified records
     */
    @Override
    public void init(final FixedKeyProcessorContext<String, JsonObject> fixedKeyProcessorContext) {
        this.context = fixedKeyProcessorContext;
    }

    /**
     * Processes each incoming record by injecting the offset from the record's metadata into its value.
     * The offset is added as a new property to the JSON object of the record.
     *
     * @param record the incoming record containing the key and the JSON object as value
     */
    @Override
    public void process(final FixedKeyRecord<String, JsonObject> record) {
        final JsonObject value = record.value();
        context.recordMetadata()
                .ifPresent(recordMetadata -> value.addProperty(OFFSET_PROPERTY_NAME, recordMetadata.offset()));
        context.forward(record.withValue(value));
    }
}
