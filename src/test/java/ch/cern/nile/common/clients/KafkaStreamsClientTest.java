package ch.cern.nile.common.clients;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.UnknownHostException;
import java.util.Properties;

import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import ch.cern.nile.common.configuration.properties.ClientProperties;
import ch.cern.nile.common.exceptions.ReverseDnsLookupException;

import lombok.SneakyThrows;

class KafkaStreamsClientTest {

    private static final String INVALID_CLUSTER = "invalidCluster";

    private KafkaStreamsClient client;
    private Properties properties;
    private Topology topology;

    @Mock
    private KafkaStreams kafkaStreams;

    private AutoCloseable closeable;

    @BeforeEach
    public void setup() {
        closeable = MockitoAnnotations.openMocks(this);
        client = new KafkaStreamsClient() {

            @Override
            @SuppressWarnings("checkstyle:HiddenField")
            public KafkaStreams create(final Topology topology) {
                return kafkaStreams;
            }

            @Override
            protected String performDnsLookup(final String kafkaCluster) throws UnknownHostException {
                if (INVALID_CLUSTER.equals(kafkaCluster)) {
                    throw new UnknownHostException("Invalid cluster");
                }
                return "localhost:9092";
            }
        };
        properties = new Properties();
        topology = Mockito.mock(Topology.class);
    }

    @AfterEach
    @SneakyThrows
    public void tearDown() {
        closeable.close();
    }

    @Test
    void givenNonTestCluster_whenConfigure_thenKafkaStreamsCreated() {
        properties.setProperty(ClientProperties.CLIENT_ID.getValue(), "testClientId");
        properties.setProperty(ClientProperties.KAFKA_CLUSTER.getValue(), "nonTestCluster");
        properties.setProperty(ClientProperties.TRUSTSTORE_LOCATION.getValue(), "/path/to/truststore");
        properties.setProperty(StreamsConfig.SECURITY_PROTOCOL_CONFIG, "PLAINTEXT");

        client.configure(properties);

        try (KafkaStreams streams = client.create(topology)) {
            assertNotNull(streams);
        }
    }

    @Test
    void givenTestCluster_whenConfigure_thenKafkaStreamsCreated() {
        properties.setProperty(ClientProperties.CLIENT_ID.getValue(), "testClientId");
        properties.setProperty(ClientProperties.KAFKA_CLUSTER.getValue(), "test");
        properties.setProperty("bootstrap.servers", "localhost:9092");

        client.configure(properties);

        try (KafkaStreams streams = client.create(topology)) {
            assertNotNull(streams);
        }
    }

    @Test
    void givenInvalidCluster_whenConfigure_thenReverseDnsLookupExceptionThrown() {
        properties.setProperty(ClientProperties.CLIENT_ID.getValue(), "testClientId");
        properties.setProperty(ClientProperties.KAFKA_CLUSTER.getValue(), "invalidCluster");

        assertThrows(ReverseDnsLookupException.class, () -> client.configure(properties));
    }

    @Test
    void givenKnownDomain_whenPerformDnsLookup_thenResultContainsPort9093() throws UnknownHostException {
        final String domain = "www.google.com";
        final String result = new KafkaStreamsClient().performDnsLookup(domain);

        assertNotNull(result);
        assertTrue(result.contains(":9093"));
    }

    @Test
    void givenLocalhost_whenPerformDnsLookup_thenResultContainsPort9093() throws UnknownHostException {
        final String domain = "localhost";
        final String result = new KafkaStreamsClient().performDnsLookup(domain);

        assertNotNull(result);
        assertTrue(result.contains(":9093"));
    }

}
