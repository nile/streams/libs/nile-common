package ch.cern.nile.common.configuration;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Properties;

import org.junit.jupiter.api.Test;

import ch.cern.nile.common.configuration.properties.ClientProperties;
import ch.cern.nile.common.configuration.properties.CommonProperties;
import ch.cern.nile.common.configuration.properties.DecodingProperties;
import ch.cern.nile.common.configuration.properties.EnrichmentProperties;
import ch.cern.nile.common.configuration.properties.RoutingProperties;
import ch.cern.nile.common.exceptions.MissingPropertyException;

class PropertiesCheckTest {

    @Test
    void givenNullProperties_whenValidateProperties_thenThrowsRuntimeException() {
        assertThrows(RuntimeException.class, () -> PropertiesCheck.validateProperties(null, StreamType.ROUTING));
    }

    @Test
    void givenNullStreamType_whenValidateProperties_thenThrowsRuntimeException() {
        final Properties properties = new Properties();

        assertThrows(RuntimeException.class, () -> PropertiesCheck.validateProperties(properties, null));
    }

    @Test
    void givenValidDecodingProperties_whenValidateProperties_thenPassesValidation() {
        final Properties properties = new Properties();
        initClientAndCommonProperties(properties);
        properties.put(DecodingProperties.SINK_TOPIC.getValue(), "");

        assertDoesNotThrow(() -> PropertiesCheck.validateProperties(properties, StreamType.DECODING));
    }

    @Test
    void givenValidRoutingProperties_whenValidateProperties_thenPassesValidation() {
        final Properties properties = new Properties();
        initClientAndCommonProperties(properties);
        properties.put(RoutingProperties.ROUTING_CONFIG_PATH.getValue(), "");
        properties.put(RoutingProperties.DLQ_TOPIC.getValue(), "");

        assertDoesNotThrow(() -> PropertiesCheck.validateProperties(properties, StreamType.ROUTING));
    }

    @Test
    void givenValidEnrichmentProperties_whenValidateProperties_thenPassesValidation() {
        final Properties properties = new Properties();
        initClientAndCommonProperties(properties);
        properties.put(EnrichmentProperties.ENRICHMENT_CONFIG_PATH.getValue(), "");
        properties.put(EnrichmentProperties.SINK_TOPIC.getValue(), "");

        assertDoesNotThrow(() -> PropertiesCheck.validateProperties(properties, StreamType.ENRICHMENT));
    }

    @Test
    void givenMissingRequiredProperty_whenValidateProperties_thenThrowsMissingPropertyException() {
        final Properties properties = new Properties();
        initClientAndCommonProperties(properties);
        // Remove a required property for routing, for example
        properties.remove(RoutingProperties.ROUTING_CONFIG_PATH.getValue());

        assertThrows(MissingPropertyException.class,
                () -> PropertiesCheck.validateProperties(properties, StreamType.ROUTING));
    }

    private void initClientAndCommonProperties(final Properties properties) {
        properties.put(ClientProperties.CLIENT_ID.getValue(), "");
        properties.put(ClientProperties.KAFKA_CLUSTER.getValue(), "");
        properties.put(ClientProperties.SOURCE_TOPIC.getValue(), "");
        properties.put(ClientProperties.TRUSTSTORE_LOCATION.getValue(), "");
        properties.put(CommonProperties.STREAM_CLASS.getValue(), "");
        properties.put(CommonProperties.STREAM_TYPE.getValue(), "");
    }

}
