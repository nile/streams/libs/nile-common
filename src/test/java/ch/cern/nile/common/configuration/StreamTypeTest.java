package ch.cern.nile.common.configuration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

class StreamTypeTest {

    @Test
    void givenKnownStreamTypeRouting_whenFindByValue_thenMapsToRouting() {
        final StreamType result = StreamType.valueOf("ROUTING");

        assertEquals(StreamType.ROUTING, result);
    }

    @Test
    void givenKnownStreamTypeDecoding_whenFindByValue_thenMapsToDecoding() {
        final StreamType result = StreamType.valueOf("DECODING");

        assertEquals(StreamType.DECODING, result);
    }

    @Test
    void givenKnownStreamTypeEnrichment_whenFindByValue_thenMapsToEnrichment() {
        final StreamType result = StreamType.valueOf("ENRICHMENT");

        assertEquals(StreamType.ENRICHMENT, result);
    }

    @Test
    void givenUnknownStreamType_whenFindByValue_thenThrowsIllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> StreamType.valueOf("Unknown"));
    }
}
