package ch.cern.nile.common.configuration.properties;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Set;

import org.junit.jupiter.api.Test;

class StreamConfigTest {

    private static final String UNKNOWN_PROPERTY = "unknown.property";

    @Test
    void givenClientPropertiesEnum_whenGetValues_thenReturnsExpectedSet() {
        final Set<String> expectedConfigs = Set.of("source.topic", "kafka.cluster", "client.id", "truststore.location");
        final Set<String> actualConfigs = PropertyEnum.getValues(ClientProperties.class);

        assertEquals(expectedConfigs, actualConfigs);
    }

    @Test
    void givenClientPropertiesEnum_whenValueOfWithUnknownProperty_thenThrowsIllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> ClientProperties.valueOf(UNKNOWN_PROPERTY));
    }

    @Test
    void givenCommonPropertiesEnum_whenGetValues_thenReturnsExpectedSet() {
        final Set<String> expectedConfigs = Set.of("stream.type", "stream.class");
        final Set<String> actualConfigs = PropertyEnum.getValues(CommonProperties.class);

        assertEquals(expectedConfigs, actualConfigs);
    }

    @Test
    void givenCommonPropertiesEnum_whenValueOfWithUnknownProperty_thenThrowsIllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> CommonProperties.valueOf(UNKNOWN_PROPERTY));
    }

    @Test
    void givenDecodingPropertiesEnum_whenGetValues_thenReturnsExpectedSet() {
        final Set<String> expectedConfigs = Set.of("sink.topic");
        final Set<String> actualConfigs = PropertyEnum.getValues(DecodingProperties.class);

        assertEquals(expectedConfigs, actualConfigs);
    }

    @Test
    void givenDecodingPropertiesEnum_whenValueOfWithUnknownProperty_thenThrowsIllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> DecodingProperties.valueOf(UNKNOWN_PROPERTY));
    }

    @Test
    void givenRoutingPropertiesEnum_whenGetValues_thenReturnsExpectedSet() {
        final Set<String> expectedConfigs = Set.of("routing.config.path", "dlq.topic");
        final Set<String> actualConfigs = PropertyEnum.getValues(RoutingProperties.class);

        assertEquals(expectedConfigs, actualConfigs);
    }

    @Test
    void givenRoutingPropertiesEnum_whenValueOfWithUnknownProperty_thenThrowsIllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> RoutingProperties.valueOf(UNKNOWN_PROPERTY));
    }

    @Test
    void givenEnrichmentPropertiesEnum_whenGetValues_thenReturnsExpectedSet() {
        final Set<String> expectedConfigs = Set.of("enrichment.config.path", "sink.topic");
        final Set<String> actualConfigs = PropertyEnum.getValues(EnrichmentProperties.class);

        assertEquals(expectedConfigs, actualConfigs);
    }

    @Test
    void givenEnrichmentPropertiesEnum_whenValueOfWithUnknownProperty_thenThrowsIllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> EnrichmentProperties.valueOf(UNKNOWN_PROPERTY));
    }

}
