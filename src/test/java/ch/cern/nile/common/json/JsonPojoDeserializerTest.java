package ch.cern.nile.common.json;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

import ch.cern.nile.common.models.Application;
import ch.cern.nile.common.models.Topic;

class JsonPojoDeserializerTest {

    private static final String TEST_TOPIC = "test-topic";
    private final JsonPojoDeserializer<Application> applicationDeserializer =
            new JsonPojoDeserializer<>(Application.class);
    private final JsonPojoDeserializer<Topic> topicDeserializer = new JsonPojoDeserializer<>(Topic.class);

    @Test
    void givenJsonWithApplication_whenDeserialize_thenReturnsApplication() {
        final String json = "{\"name\":\"my-app\",\"topic\":{\"name\":\"my-topic\"}}";
        final Application expected = new Application();
        expected.setName("my-app");
        expected.setTopic(new Topic());
        expected.getTopic().setName("my-topic");
        final Application actual = applicationDeserializer.deserialize(TEST_TOPIC, json.getBytes());

        assertEquals(expected.toString(), actual.toString());
    }

    @Test
    void givenJsonWithTopic_whenDeserialize_thenReturnsTopic() {
        final String json = "{\"name\":\"my-topic\"}";

        final Topic expected = new Topic();
        expected.setName("my-topic");
        final Topic actual = topicDeserializer.deserialize(TEST_TOPIC, json.getBytes());

        assertEquals(expected.toString(), actual.toString());
    }

    @Test
    void givenNullBytes_whenDeserialize_thenReturnsNull() {
        assertNull(applicationDeserializer.deserialize(TEST_TOPIC, null));
    }

    @Test
    void givenNullJson_whenDeserialize_thenReturnsNull() {
        assertNull(applicationDeserializer.deserialize(TEST_TOPIC, "null".getBytes()));
    }

}
