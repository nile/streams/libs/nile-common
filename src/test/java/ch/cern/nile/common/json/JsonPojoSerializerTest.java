package ch.cern.nile.common.json;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

class JsonPojoSerializerTest {

    @Test
    void givenEmptyConfig_whenConfigure_thenDoesNotThrowException() {
        try (JsonPojoSerializer<Object> serializer = new JsonPojoSerializer<>()) {
            assertDoesNotThrow(() -> serializer.configure(Collections.emptyMap(), true));
        }
    }

    @Test
    void givenNullData_whenSerialize_thenReturnsNull() {
        try (JsonPojoSerializer<Object> serializer = new JsonPojoSerializer<>()) {
            assertNull(serializer.serialize("topic", null));
        }
    }

    @Test
    void givenNonNullData_whenSerialize_thenReturnsJsonBytes() {
        final Map<String, String> data = new HashMap<>();
        data.put("key", "value");

        final byte[] expectedBytes = "{\"key\":\"value\"}".getBytes();

        try (JsonPojoSerializer<Map<String, String>> serializer = new JsonPojoSerializer<>()) {
            final byte[] actualBytes = serializer.serialize("topic", data);

            assertArrayEquals(expectedBytes, actualBytes);
        }
    }

    @Test
    @SuppressWarnings("EmptyTryBlock")
    void givenSerializer_whenClosed_thenDoesNotThrowException() {
        assertDoesNotThrow(() -> {
            try (JsonPojoSerializer<Object> ignored = new JsonPojoSerializer<>()) {
            }
        });
    }
}
