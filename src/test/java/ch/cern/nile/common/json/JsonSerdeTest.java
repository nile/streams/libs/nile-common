package ch.cern.nile.common.json;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

class JsonSerdeTest {

    @Test
    void givenEmptyConfigs_whenConfigure_thenSerializerAndDeserializerNotNull() {
        try (JsonSerde jsonSerde = new JsonSerde()) {

            final Map<String, Object> configs = new HashMap<>();
            configs.put("config-key", "config-value");
            jsonSerde.configure(configs, true);

            assertNotNull(jsonSerde.serializer());
            assertNotNull(jsonSerde.deserializer());
        }
    }
}
