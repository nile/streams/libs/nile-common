package ch.cern.nile.common.probes;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.InetSocketAddress;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import org.apache.kafka.streams.KafkaStreams;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

class HealthTest {

    private static final int PORT = 8899;
    private static final int OK_RESPONSE = 200;
    private static final int ERROR_RESPONSE = 500;

    private KafkaStreams mockStreams;
    private HttpServer mockServer;
    private HttpServerFactory mockFactory;

    private Health health;

    @BeforeEach
    void before() throws IOException {
        mockStreams = mock(KafkaStreams.class);
        mockServer = mock(HttpServer.class);
        mockFactory = mock(HttpServerFactory.class);
        when(mockFactory.createHttpServer(any(InetSocketAddress.class), anyInt())).thenReturn(mockServer);

        health = new Health(mockStreams, mockFactory);
    }

    @Test
    void givenHealthServer_whenStart_thenServerStartsAndCreatesHealthContext() throws IOException {
        health.start();

        verify(mockFactory).createHttpServer(new InetSocketAddress(PORT), 0);
        verify(mockServer).createContext(eq("/health"), any(HttpHandler.class));
        verify(mockServer).start();
    }

    @Test
    void givenHealthServer_whenStop_thenServerStops() {
        health.start();
        health.stop();

        verify(mockServer).stop(0);
    }

    @Test
    @SuppressWarnings("PMD.CloseResource")
    @SuppressFBWarnings(value = "CloseResource", justification = "Mocked HttpServer")
    void givenKafkaStreamsRunning_whenHealthCheck_thenResponseStatus200() throws IOException {
        when(mockStreams.state()).thenReturn(KafkaStreams.State.RUNNING);
        health.start();

        final ArgumentCaptor<HttpHandler> handlerCaptor = ArgumentCaptor.forClass(HttpHandler.class);
        verify(mockServer).createContext(eq("/health"), handlerCaptor.capture());

        final HttpExchange mockExchange = mock(HttpExchange.class);
        handlerCaptor.getValue().handle(mockExchange);
        verify(mockExchange).sendResponseHeaders(OK_RESPONSE, 0);
        verify(mockExchange).close();
    }

    @Test
    @SuppressWarnings("PMD.CloseResource")
    @SuppressFBWarnings(value = "CloseResource", justification = "Mocked HttpServer")
    void givenKafkaStreamsNotRunning_whenHealthCheck_thenResponseStatus500() throws IOException {
        when(mockStreams.state()).thenReturn(KafkaStreams.State.NOT_RUNNING);
        health.start();

        final ArgumentCaptor<HttpHandler> handlerCaptor = ArgumentCaptor.forClass(HttpHandler.class);
        verify(mockServer).createContext(eq("/health"), handlerCaptor.capture());

        final HttpExchange mockExchange = mock(HttpExchange.class);
        handlerCaptor.getValue().handle(mockExchange);
        verify(mockExchange).sendResponseHeaders(ERROR_RESPONSE, 0);
        verify(mockExchange).close();
    }

    @Test
    void givenHttpServerCreationFails_whenStart_thenThrowsRuntimeException() throws IOException {
        when(mockFactory.createHttpServer(any(InetSocketAddress.class), anyInt())).thenThrow(IOException.class);

        assertThrows(RuntimeException.class, () -> health.start());
    }
}
