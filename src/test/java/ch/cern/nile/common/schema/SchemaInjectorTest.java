package ch.cern.nile.common.schema;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class SchemaInjectorTest {

    private static final String TIMESTAMP_COL = "timestamp_col";
    private static final String TIMESTAMP_TYPE = "org.apache.kafka.connect.data.Timestamp";
    private static final Map<String, Object> DATA = new HashMap<>();
    private static final String DATE_COL = "date_col";

    @BeforeAll
    public static void before() {
        DATA.put("byte_col", (byte) 1);
        DATA.put("short_col", (short) 2);
        DATA.put("int_col", 3);
        DATA.put("long_col", (long) 4);
        DATA.put("float_col", 5.0f);
        DATA.put("double_col", 6.0);
        DATA.put("boolean_col", true);
        DATA.put("string_col", "test");
        DATA.put("timestamp_col", 1_501_834_166_000L);
        DATA.put(DATE_COL, new Date());
        DATA.put("bytes_col", new byte[]{1, 2, 3});
    }

    @Test
    void givenValidInputData_whenInject_thenReturnsCorrectSchemaAndPayload() {
        final Map<String, Object> result = SchemaInjector.inject(DATA);

        assertNotNull(result);

        assertTrue(result.containsKey("schema"));
        assertTrue(result.containsKey("payload"));

        final Map<String, Object> schema = (Map<String, Object>) result.get("schema");
        assertEquals("struct", schema.get("type"));
        final List<Map<String, Object>> fields = (List<Map<String, Object>>) schema.get("fields");
        assertEquals(DATA.size(), fields.size());

        for (final Map<String, Object> field : fields) {
            final String fieldName = (String) field.get("field");
            assertTrue(DATA.containsKey(fieldName));
            assertNotNull(field.get("type"));

            if (TIMESTAMP_COL.equals(fieldName)) {
                assertFalse(Boolean.parseBoolean(field.get("optional").toString()));
            } else {
                assertTrue(Boolean.parseBoolean(field.get("optional").toString()));
            }

            if (TIMESTAMP_COL.equals(fieldName)) {
                assertEquals(TIMESTAMP_TYPE, field.get("name"));
                assertEquals(1, field.get("version"));
            } else if (DATE_COL.equals(fieldName)) {
                assertEquals("org.apache.kafka.connect.data.Date", field.get("name"));
                assertEquals(1, field.get("version"));
            }
        }

        final Map<String, Object> payload = (Map<String, Object>) result.get("payload");
        assertEquals(DATA, payload);
    }

    @Test
    void givenDataWithNullValue_whenInject_thenThrowsIllegalArgumentException() {
        final Map<String, Object> data = new HashMap<>(DATA);
        data.put("nullValue", null);

        assertThrows(IllegalArgumentException.class, () -> SchemaInjector.inject(data));
    }

    @Test
    void givenDataWithUnsupportedType_whenInject_thenThrowsIllegalArgumentException() {
        final Map<String, Object> data = new HashMap<>(DATA);
        data.put("unsupportedType", new Object());

        assertThrows(IllegalArgumentException.class, () -> SchemaInjector.inject(data));
    }
}
