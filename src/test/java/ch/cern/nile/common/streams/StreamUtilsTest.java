package ch.cern.nile.common.streams;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.junit.jupiter.api.Test;

class StreamUtilsTest {

    private static final String KEY = "key";

    @Test
    void givenValidGatewayInfo_whenAddingTimestamp_thenTimestampIsAdded() {
        final JsonArray gatewayInfo = new JsonArray();
        final JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("time", Instant.now().toString());
        gatewayInfo.add(jsonObject);

        final Map<String, Object> map = new HashMap<>();
        StreamUtils.addTimestamp(gatewayInfo, map);

        assertTrue(map.containsKey("timestamp"));
    }

    @Test
    void givenMissingAllGatewayTimestamps_whenAddingTimestamp_thenCurrentTimestampIsAdded() {
        final JsonArray gatewayInfo = new JsonArray();
        final Map<String, Object> map = new HashMap<>();

        StreamUtils.addTimestamp(gatewayInfo, map);
        assertTrue(map.containsKey("timestamp"));
    }

    @Test
    void givenNonNullValue_whenFilteringNull_thenReturnsTrue() {
        assertTrue(StreamUtils.filterNull(KEY, new Object()));
    }

    @Test
    void givenNullValue_whenFilteringNull_thenReturnsFalse() {
        assertFalse(StreamUtils.filterNull(KEY, null));
    }

    @Test
    void givenNonEmptyList_whenFilteringEmpty_thenReturnsTrue() {
        assertTrue(StreamUtils.filterEmpty(KEY, List.of(1, 2, 3)));
    }

    @Test
    void givenEmptyList_whenFilteringEmpty_thenReturnsFalse() {
        assertFalse(StreamUtils.filterEmpty(KEY, List.of()));
    }

    @Test
    void givenNonEmptyMap_whenFilteringEmpty_thenReturnsTrue() {
        final Map<String, String> nonEmptyMap = new HashMap<>();
        nonEmptyMap.put(KEY, "value");
        assertTrue(StreamUtils.filterEmpty(KEY, nonEmptyMap));
    }

    @Test
    void givenEmptyMap_whenFilteringEmpty_thenReturnsFalse() {
        final Map<String, String> emptyMap = new HashMap<>();
        assertFalse(StreamUtils.filterEmpty(KEY, emptyMap));
    }

    @Test
    void givenValidJsonObject_whenFilteringRecord_thenReturnsTrue() {
        final JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("applicationID", "appId");
        jsonObject.addProperty("applicationName", "appName");
        jsonObject.addProperty("deviceName", "deviceName");
        jsonObject.addProperty("devEUI", "devEUI");
        jsonObject.addProperty("data", "data");

        assertTrue(StreamUtils.filterRecord(KEY, jsonObject));
    }

    @Test
    void givenInvalidJsonObject_whenFilteringRecord_thenReturnsFalse() {
        final JsonObject jsonObject = new JsonObject();

        assertFalse(StreamUtils.filterRecord(KEY, jsonObject));
    }
}
