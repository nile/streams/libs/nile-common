package ch.cern.nile.common.streams.offsets;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;

import java.util.Set;

import org.apache.kafka.streams.state.StoreBuilder;
import org.junit.jupiter.api.Test;

class InjectOffsetProcessorSupplierTest {

    @Test
    void whenGetCalled_thenReturnInjectOffsetTransformer() {
        final InjectOffsetProcessorSupplier supplier = new InjectOffsetProcessorSupplier();
        assertInstanceOf(InjectOffsetTransformer.class, supplier.get());
    }

    @Test
    void whenRequestingStores_thenCorrectStoreConfigured() {
        final InjectOffsetProcessorSupplier supplier = new InjectOffsetProcessorSupplier();
        final Set<StoreBuilder<?>> stores = supplier.stores();
        assertFalse(stores.isEmpty());
        assertEquals(stores.iterator().next().name(), InjectOffsetProcessorSupplier.getSTORE_NAME());
    }

}
