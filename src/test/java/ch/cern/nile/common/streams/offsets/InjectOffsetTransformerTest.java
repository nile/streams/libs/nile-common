package ch.cern.nile.common.streams.offsets;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import com.google.gson.JsonObject;

import org.apache.kafka.streams.processor.api.FixedKeyProcessorContext;
import org.apache.kafka.streams.processor.api.FixedKeyRecord;
import org.apache.kafka.streams.processor.api.RecordMetadata;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class InjectOffsetTransformerTest {
    private InjectOffsetTransformer transformer;
    private FixedKeyProcessorContext<String, JsonObject> context;

    @Mock
    private RecordMetadata metadata;
    @Mock
    private FixedKeyRecord<String, JsonObject> record;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        transformer = new InjectOffsetTransformer();
        context = mock(FixedKeyProcessorContext.class);
        when(context.recordMetadata()).thenReturn(Optional.of(metadata));
        when(metadata.offset()).thenReturn(123L);
        transformer.init(context);
    }

    @Test
    void givenMetadata_whenProcessRecord_thenInjectOffset() {
        final JsonObject value = new JsonObject();
        when(record.value()).thenReturn(value);
        when(record.withValue(value)).thenReturn(record);

        transformer.process(record);

        verify(context).forward(record);
        assertEquals("123", value.get("offset").getAsString());
    }
}